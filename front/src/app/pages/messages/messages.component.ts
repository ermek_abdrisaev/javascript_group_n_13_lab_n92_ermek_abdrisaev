import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { Message, ServerMessage } from '../../models/message.model';
import { fetchUsersRequest } from '../../store/users.actions';
import { Observable } from 'rxjs';
import { User, Users } from '../../models/user.model';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  messageText = '';
  messages: Message[] = [];
  ws!: WebSocket;
  user: Observable<null | User>;
  users: Observable<Users[]>;
  displayName!: string;
  userToken!: string;

  constructor(private store: Store<AppState>) {
    this.users = store.select(state => state.users.users);
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.ws = new WebSocket('ws://localhost:8080/chat');

    this.ws.onclose = () => {
      setTimeout(() =>{
        this.ws = new WebSocket('ws://localhost:8080/chat');
      });
    }

    this.user.subscribe(user =>{
      this.displayName = <string>user?.displayName;
      this.userToken = <string>user?.token;
    })

    this.ws.onmessage = event => {
      const decodedMessage: ServerMessage = JSON.parse(event.data);

      if(decodedMessage.type === 'NEW_MESSAGE'){
        this.messages = this.messages.concat(decodedMessage.message);
        this.store.dispatch(fetchUsersRequest());
      }
    };

    this.ws.onopen = () =>{
      this.setUsername();
    }
  }

  setUsername(){
    this.ws.send(JSON.stringify({
      type: 'SET_USER',
      displayName: this.displayName,
      token: this.userToken,
    }));
  }

  sendMessage() {
    this.ws.send(JSON.stringify({
      type: 'SEND_MESSAGE',
      message: this.messageText
    }));
    this.messageText = '';
  }

  ngOnDestroy(): void{
    this.ws.close();
  }
}
