import { LoginError, RegisterError, User, Users } from '../models/user.model';

export type UsersState = {
  user: null | User,
  users: Users[],
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError,
  fetchLoading: boolean,
  fetchError: null | string,
}

export type AppState ={
  users: UsersState,
 }
