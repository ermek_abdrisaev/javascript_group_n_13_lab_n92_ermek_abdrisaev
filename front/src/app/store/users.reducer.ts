import { createReducer, on } from '@ngrx/store';
import {
  fetchUsersFailure,
  fetchUsersRequest, fetchUsersSuccess,
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess, logoutUser,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from './users.actions';
import { UsersState } from './types';

const initialState: UsersState ={
  user: null,
  users: [],
  registerLoading: false,
  registerError: null,
  loginLoading: false,
  loginError: null,
  fetchLoading: false,
  fetchError: null,
};

export const usersReducer = createReducer(
  initialState,
  on(registerUserRequest, state => ({...state, registerLoading: true, registerError: null})),
  on(registerUserSuccess, (state, {user}) => ({...state, registerLoading: false, user})),
  on(registerUserFailure, (state, {error}) =>({...state, registerLoading: false, registerError: error})),

  on(loginUserRequest, state => ({...state, loginLoading: true, loginError: null})),
  on(loginUserSuccess, (state, {user}) => ({...state, loginLoading: false, user})),
  on(loginUserFailure, (state, {error}) => ({...state, loginLoading: false, loginError: error})),

  on(fetchUsersRequest, state => ({...state, fetchLoading: true, fetchError: null})),
  on(fetchUsersSuccess,(state, {users}) => ({...state, fetchLoading: false, users})),
  on(fetchUsersFailure, (state, {error}) => ({...state, fetchLoading: false, fetchError: error})),

  on(logoutUser, state => ({...state, user: null,}))
)
