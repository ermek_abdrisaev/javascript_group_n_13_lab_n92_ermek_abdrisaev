import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';
import {
  fetchUsersFailure,
  fetchUsersRequest, fetchUsersSuccess,
  loginUserFailure,
  loginUserRequest,
  loginUserSuccess,
  logoutUser,
  logoutUserRequest,
  registerUserFailure,
  registerUserRequest,
  registerUserSuccess
} from './users.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { HelpersService } from '../services/helpers.service';
import { AppState } from './types';
import { Store } from '@ngrx/store';

@Injectable()
export class UsersEffects {
  constructor(
    private actions: Actions,
    private usersService: UsersService,
    private router: Router,
    private helpers: HelpersService,
    private store: Store<AppState>
  ) {
  }

  registerUser = createEffect(() => this.actions.pipe(
    ofType(registerUserRequest),
    mergeMap(({userData}) => this.usersService.registerUser(userData).pipe(
      map(user => registerUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackBar('Register successful', 'Got it!');
        void this.router.navigate(['/messages']);
      }),
      this.helpers.catchServerError(registerUserFailure)
    ))
  ));

  loginUser = createEffect(() => this.actions.pipe(
    ofType(loginUserRequest),
    mergeMap(({userData}) => this.usersService.login(userData).pipe(
      map(user => loginUserSuccess({user})),
      tap(() => {
        this.helpers.openSnackBar('Login successful');
        void this.router.navigate(['/messages']);
      }),
      this.helpers.catchServerError(loginUserFailure)
    ))
  ));

  logoutUser = createEffect(() => this.actions.pipe(
    ofType(logoutUserRequest),
    mergeMap(({token}) => {
      return this.usersService.logout(token).pipe(
        map(() => logoutUser()),
        tap(() => {
          void this.router.navigate(['/']);
          this.helpers.openSnackBar('Logout successful');
        })
      );
    }))
  );

  fetchUsers = createEffect(() => this.actions.pipe(
    ofType(fetchUsersRequest),
    mergeMap(() => this.usersService.getUser().pipe(
      map(users => fetchUsersSuccess({users})),
      catchError(() =>{
        return of(fetchUsersFailure({error: 'Error'}));
      })
    ))
  ))
}
