export interface Message {
  username: string,
  text: string
}

export interface ServerMessage{
  type: string,
  message: Message
}
