import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiUser, LoginUserData, RegisterUserData, User, Users } from '../models/user.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) {
  }

  getUser(){
    return this.http.get<ApiUser[]>(environment.apiUrl + '/users/').pipe(
      map(response =>{
        return response.map(users =>{
          return new Users(users.name);
        });
      })
    )
  }

  registerUser(userData: RegisterUserData) {
    return this.http.post<User>(environment.apiUrl + '/users', userData);
  }

  login(userData: LoginUserData) {
    return this.http.post<User>(environment.apiUrl + '/users/sessions', userData);
  }

  logout(token: string) {
    return this.http.delete(environment.apiUrl + '/users/sessions',
      {headers: new HttpHeaders({'Authorization': token})});
  }
}
