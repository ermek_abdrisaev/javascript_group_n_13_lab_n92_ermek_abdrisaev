const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const mongoose = require('mongoose');
const config = require('./config');
const User = require('./models/User');
const users = require('./app/users')
const messages = require('./models/Messages');
const Message = require("./models/Messages");
const app = express();
require('express-ws')(app);
const port = 8080;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use('/users', users);

const activeConnections = {};

app.ws('/chat', (ws, req) => {
  const id = nanoid();
  activeConnections[id] = ws;

  let user = {
    displayName: String,
    token: String
  }

  ws.on('message', async msg => {
    const decodedMessage = JSON.parse(msg);
    if (decodedMessage.type === 'SEND_MESSAGE') {
      let msDt = {
        user: user.displayName,
        message: decodedMessage.message
      }
      const message = new Message(msDt);
      await message.save();
    }

      switch (decodedMessage.type) {
        case 'SET_USER':
          user.displayName = decodedMessage.displayName;
          user.token = decodedMessage.token;
          let msDt = {
            user: user.displayName,
            message: 'Connected!'
          }

          const message = new Message(msDt);

          await message.save();

          const messages = await Message.find().limit(30);

          Object.keys(activeConnections).forEach(id => {
            const conn = activeConnections[id];
            conn.send(JSON.stringify({
              type: 'NEW_MESSAGE',
              message: message,
            }))
          });
          break;
        case 'SEND_MESSAGE':
          const messageSnd = await Message.find();

          Object.keys(activeConnections).forEach(id => {
            const conn = activeConnections[id];
            conn.send(JSON.stringify({
              type: 'NEW_MESSAGE',
              message: messageSnd
            }))
          });
          break;
        default:
      }
    }
  );

    ws.on('close', async () => {
      let msDt = {
        user: user.displayName,
        message: 'Connection is off!'
      }
      await User.updateOne({token: user.token}, {isActive: false});

      const message = new Message(msDt);

      await message.save();

      const messages = await Message.find();

      Object.keys(activeConnections).forEach(id => {
        const conn = activeConnections[id];
        conn.send(JSON.stringify({
          type: 'NEW_MESSAGE',
          message: messages
        }));
      });
      delete activeConnections[id];
    });
  });

  const run = async () => {
    await mongoose.connect(config.mongo.db, config.mongo.options);

    app.listen(port, () => {
      console.log(`Server started on ${port} port!`);
    });

    process.on('exit', () => {
      mongoose.disconnect();
    });
  };

  run().catch(e => console.error(e));