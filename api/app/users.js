const express = require('express');
const User = require('../models/User');
const mongoose = require("mongoose");
const auth = require("../middlwear/auth");

const router = express.Router();

router.get('/', async (req, res, next) => {
  try {
    const users = await User.find({isActive: true});
    res.send(users);
    console.log(users)
  } catch (e) {
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    const userData = {
      email: req.body.email,
      password: req.body.password,
      displayName: req.body.displayName,
    }

    const user = new User(userData);
    user.generateToken();
    await user.save();

    return res.send(user);

  } catch (e) {
    if (e instanceof mongoose.Error.ValidationError) {
      return res.status(400).send(e);
    }
    return next(e);
  }
});

router.post('/sessions', async (req, res, next) => {
  try {
    await User.updateOne({email: req.body.email}, {isActive: true});
    const user = await User.findOne({email: req.body.email});
    if (!user) {
      return res.status(400).send({error: 'Email not found'});
    }
    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {

      return res.status(400).send({error: 'Password is wrong'});
    }
    user.generateToken();

    await user.save();

    return res.send(user);
  } catch (e) {
    next(e);
  }
});

router.get('/secret', auth, async (req, res, next) => {
  try {
    return res.send({message: 'Hello, ' + req.user.email});
  } catch (e) {
    next(e);
  }
});

router.delete('/sessions', async (req, res, next) => {
  try {
    const token = req.get('Authorization');
    const message = {message: 'OK'};
    if(!token) return res.send(message);

    await Users.updateOne({token: token}, {isActive: false});
    const user = await User.findOne({token});
    if (!user) return res.send(message);
    user.generateToken();
    await user.save();

    return res.send(message);
  } catch (e) {
    next(e);
  }
})

module.exports = router;