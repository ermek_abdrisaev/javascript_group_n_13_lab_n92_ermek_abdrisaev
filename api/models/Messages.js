const mongoose = require('mongoose');
const Schema = mongoose. Schema;

const MessagesSchema = new Schema({
  message: {
    type: String,
  },
  user: {
    type: String,
  },
});

const Message = mongoose.model('Message', MessagesSchema);
module.exports = Message;